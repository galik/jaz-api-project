<%@ page import="domain.Weather" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Insert title here</title>
</head>
<body>

    <jsp:useBean id="weatherSession" class="domain.Weather" scope="session"></jsp:useBean>
    <jsp:setProperty name="weatherSession" property="*"></jsp:setProperty>
    <jsp:useBean id="apiService" class="services.WeatherApiService" scope="application"></jsp:useBean>

    <%
        Weather weather = apiService.getWeatherInfo(weatherSession.getCity());
    %>

    <p>
        Wind speed: <%= weather.getWind().getSpeed() %>
    </p>
    <p>
        Clouds : <%= weather.getClouds().getAll() %> %
    </p>
    <p>
        Temperature: <%= weather.getMain().getTemp() - 273 %> C
    </p>
    <p>
        Pressure: <%= weather.getMain().getPressure() %> hPa
    </p>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

    <jsp:useBean id="apiService" class="services.WeatherApiService" scope="application"></jsp:useBean>
    <jsp:useBean id="weather" class="domain.Weather" scope="session"></jsp:useBean>

    <form action="getWeather.jsp">
        City: <input type="text" name="city" value="$(weather.city)" />
        <br>
        <input type="submit" value="ok" />
    </form>
</body>
</html>
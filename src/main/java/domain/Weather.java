package domain;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by galik on 23.04.2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Weather {

    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Wind {

        private String speed;

        public String getSpeed() {
            return speed;
        }

        public void setSpeed(String speed) {
            this.speed = speed;
        }

    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Clouds {

        private Integer all;

        public Integer getAll() {
            return all;
        }

        public void setAll(Integer all) {
            this.all = all;
        }

    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public class main {

        private Integer temp;
        private Integer pressure;

        public Integer getPressure() {
            return pressure;
        }

        public void setPressure(Integer pressure) {
            this.pressure = pressure;
        }

        public Integer getTemp() {
            return temp;
        }

        public void setTemp(Integer temp) {
            this.temp = temp;
        }
    }

    private String city;
    private Clouds clouds;
    private main main;
    private Wind wind;


    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Clouds getClouds() {
        return clouds;
    }

    public void setClouds(Clouds clouds) {
        this.clouds = clouds;
    }

    public main getMain() {
        return main;
    }

    public void setMain(main main) {
        this.main = main;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }
}


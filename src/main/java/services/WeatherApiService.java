package services;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import domain.Weather;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

/**
 * Created by galik on 23.04.2016.
 */
public class WeatherApiService {

    private String url = "http://api.openweathermap.org/data/2.5/weather?APPID=381cf80aa623255b49df2a0d63b145fd&q=";
    private Weather weather;

    public Weather getWeatherInfo(String city) throws IOException {
        ClientConfig clientConfig = new DefaultClientConfig();
        Client client = Client.create(clientConfig);

        WebResource webResource = client.resource(url+city);

        String response = webResource.get(String.class);
        System.out.println(response);

        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(response, Weather.class);
    }

}
